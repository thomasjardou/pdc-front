import type { Config } from 'tailwindcss'

export default <Partial<Config>>{
  theme: {
    extend: {
        fontFamily: {
            sans: ['Sora', 'sans-serif'],
        },
    }
  }
}
