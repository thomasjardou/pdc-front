export default defineNuxtConfig({
  modules: [
    "@nuxtjs/fontaine",
    "@nuxtjs/google-fonts",
    "@nuxt/ui",
    "@nuxt/image",
    "nuxt-directus"

  ],
  extends: ["@nuxt/ui-pro"],
  runtimeConfig: {
    public: {
      baseUrl: process.env.BASE_URL || "http://localhost:3001",
    },
  },
  colorMode: {
    preference: "light",
  },
  devtools: { enabled: true },
  googleFonts: {
    display: "swap",
    download: true,
    families: {
      Sora: [400, 500, 600, 700],
    },
  },
  fontMetrics: {
    fonts: ["Sora"],
  },
  ui: {
    icons: ["material-symbols", "ri"],
  },
  directus: {
    url: "http://localhost:8055/"
  }
});
